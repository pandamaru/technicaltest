//
//  SurveyModel.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import Foundation
import SwiftyJSON

class SurveyModel {
    var cover_image_url: String?
    var title: String?
    var description: String?
    
    init() {
    }
    
    init(json: JSON) {
        self.cover_image_url = json["cover_image_url"].stringValue
        self.title = json["title"].stringValue
        self.description = json["description"].stringValue
    }
    
    init(prototype: SurveyModel) {
        self.copy(prototype)
    }
    
    func copy(prototype: SurveyModel) {
        self.cover_image_url = prototype.cover_image_url
        self.title = prototype.title
        self.description = prototype.description
    }
}