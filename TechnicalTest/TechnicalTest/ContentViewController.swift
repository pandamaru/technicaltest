//
//  ContentViewController.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit
import Kingfisher

class ContentViewController: UIViewController {
    @IBOutlet var bgImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descLabel: UILabel!
    
    var surveyModel: SurveyModel!
    var pageIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.surveyModel != nil) {
            self.bgImageView.kf_setImageWithURL(NSURL(string: self.surveyModel.cover_image_url!)!, placeholderImage: nil)
            self.titleLabel.text = self.surveyModel.title
            self.descLabel.text = self.surveyModel.description
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
