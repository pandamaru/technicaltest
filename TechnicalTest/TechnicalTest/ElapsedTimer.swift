//
//  ElapsedTimer.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import Foundation

class ElapsedTimer {
    
    let startTime:CFAbsoluteTime
    var endTime:CFAbsoluteTime?
    
    init() {
        startTime = CFAbsoluteTimeGetCurrent()
    }
    
    func stop() -> CFAbsoluteTime {
        endTime = CFAbsoluteTimeGetCurrent()
        
        return duration!
    }
    
    var duration:CFAbsoluteTime? {
        if let endTime = endTime {
            return endTime - startTime
        } else {
            return nil
        }
    }
}