//
//  APISingleton.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit
import SwiftyJSON

class APISingleton {
    static let instance = APISingleton()
    
    private let baseURL = "https://www-staging.usay.co/app"
    
    private init() {
    }
    
    func sealPromiseJSON(method: Alamofire.Method, APIName: String, parameters: [String : AnyObject]?) -> Promise<JSON> {
        return Promise { fulfill, reject in
            Alamofire.request(method, baseURL + "/" + APIName, parameters: parameters).responseJSON { response in
                switch response.result {
                case .Success(let data):
                    let json = JSON(data)
                    fulfill(json)
                case .Failure(let error):
                    reject(error)
                }
            }
        }
    }
    
    func getTestJSON() -> Promise<JSON> {
        return sealPromiseJSON(.GET, APIName: "surveys.json", parameters: ["access_token" : "6eebeac3dd1dc9c97a06985b6480471211a777b39aa4d0e03747ce6acc4a3369"])
    }
}
