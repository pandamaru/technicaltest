//
//  MainViewController.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit
import PromiseKit
import SwiftyJSON

class MainViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    @IBOutlet var loadIndicator: UIActivityIndicatorView!
    @IBOutlet var backView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    
    var pageViewController: UIPageViewController!
    var surveyModelList: NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageControl.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2));
        
        self.surveyModelList = NSMutableArray()
        self.getSurvey()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getSurvey() {
        self.surveyModelList.removeAllObjects()
        self.backView.subviews.forEach({
            $0.removeFromSuperview()
        })
        self.pageControl.hidden = true
        
        self.loadIndicator.startAnimating()
        APISingleton.instance.getTestJSON().then { json -> Void in
            for (_, subJson):(String, JSON) in json {
                self.surveyModelList.addObject(SurveyModel(json: subJson))
            }
        }.always {
            self.loadIndicator.stopAnimating()
        
            self.pageViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageViewController") as! UIPageViewController
            self.pageViewController.dataSource = self
            self.pageViewController.delegate = self
        
            let firstContentViewController = self.contentViewControllerAtIndex(0)
            var contentViewControllers = [ContentViewController]()
            contentViewControllers.append(firstContentViewController)
        
            self.pageViewController.setViewControllers(contentViewControllers, direction: .Forward, animated: true, completion: nil)
            self.pageViewController.view.frame = CGRect(x: 0, y: 0, width: self.backView.frame.width, height: self.backView.frame.height)
            self.backView.addSubview(self.pageViewController.view)
        
            self.pageControl.numberOfPages = self.surveyModelList.count
            self.pageControl.currentPage = 0
            self.pageControl.hidden = false
        }.error { error in
            print(error)
            UIAlertView(title: "Error", message: "Something bad happens T-T", delegate: nil, cancelButtonTitle: "Dismiss").show()
        }
    }
    
    func contentViewControllerAtIndex(index: Int) -> ContentViewController {
        if (self.surveyModelList.count == 0) {
            //Empty data
            return ContentViewController()
        }
        
        if (index < 0 || index >= self.surveyModelList.count) {
            //index out of bound
            return ContentViewController()
        }
        
        let contentViewController: ContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("ContentViewController") as! ContentViewController
        contentViewController.surveyModel = surveyModelList[index] as! SurveyModel
        contentViewController.pageIndex = index
        
        
        
        return contentViewController
    }

    @IBAction func refreshAction(sender: AnyObject) {
        if (self.loadIndicator.hidden) {
            self.getSurvey()
        }
    }
    
    @IBAction func takeSurveyAction(sender: AnyObject) {
        if (self.loadIndicator.hidden) {
            self.performSegueWithIdentifier("Navigate", sender: nil)
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "Navigate") {
            let contentViewController:ContentViewController = self.pageViewController.viewControllers![0] as! ContentViewController
            
            let blankViewController:BlankViewController = segue.destinationViewController as! BlankViewController
            blankViewController.surveyModel = contentViewController.surveyModel
        }
    }
    
    // MARK: - Page View Controller Data Source
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        let contentViewController = viewController as! ContentViewController
        var index = contentViewController.pageIndex as Int
        
        if (index < self.surveyModelList.count - 1) {
            index++
        } else {
            return nil
        }
        
        return self.contentViewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        let contentViewController = viewController as! ContentViewController
        var index = contentViewController.pageIndex as Int
        
        if (index > 0) {
            index--
        } else {
            return nil
        }
        
        return self.contentViewControllerAtIndex(index)
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if (completed) {
            let contentViewController:ContentViewController = self.pageViewController.viewControllers![0] as! ContentViewController
            self.pageControl.currentPage = contentViewController.pageIndex
        }
    }
}
