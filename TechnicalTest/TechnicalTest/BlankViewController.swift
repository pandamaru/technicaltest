//
//  BlankViewController.swift
//  TechnicalTest
//
//  Created by PandaMaru on 11/23/2558 BE.
//  Copyright © 2558 amuse. All rights reserved.
//

import UIKit

class BlankViewController: UIViewController {
    @IBOutlet var coverImageURLLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descLebel: UILabel!
    
    var surveyModel: SurveyModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (self.surveyModel != nil) {
            coverImageURLLabel.text = "cover_image_url : \(surveyModel.cover_image_url!)"
            titleLabel.text = "title : \(surveyModel.title!)"
            descLebel.text = "description : \(surveyModel.description!)"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
